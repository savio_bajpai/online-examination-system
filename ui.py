import os, sys, time, socket, logging
from lib_socket import socket_class
from lib_log import log_class

def ui_client():
    
    data =  sys.argv[1:]
    #data = int(args[0])

    if not data or data[0] == "help":
        print "The Parameter format is as follows:\n\t1.input_file_name.input\n\t2.delay\n\t3.handler_name(lower/upper/toggle)\n\nExample: python ui.py input_file.input 1 lower\n"
        sys.exit(0)
        
    elif len(data) < 4 or len(data) > 4:
        print "Incorrect number of parameters.\n\nThe Parameter format is as follows:\n\t1.input_file_name.input\n\t2.delay\n\t3.handler_name(lower/upper/toggle)\n\nExample: python ui.py input_file.input 1 lower\n"
        sys.exit(0)

    args = " ".join(data)
    print "args: ", args

    ui = None
    ui = socket_class()
    ui.create_tcp_socket( )
    ui.connect( 2022 )
    ui.send( args )
    while True:
        datas = ui.recv(1024)
        if datas:
            print "Data received:", datas
        elif not datas:
            break
    del ui

if __name__ == '__main__':
    ui_client()



