#qemu-ifup
/sbin/ifconfig enp1s0 down
/sbin/ifconfig enp1s0 0.0.0.0 promisc up
openvpn --mktun --dev tap1
openvpn --mktun --dev tap2
openvpn --mktun --dev tap3
ifconfig tap 0 0.0.0.0 up
brctl addbr br0
brctl addif br0 enp1s0
brctl addif br0 tap1 
brctl addif br0 tap2
brctl addif br0 tap3
brctl stp br0 off
ifconfig br0 10.10.10.2 netmask 255.255.255.0
