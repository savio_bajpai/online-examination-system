import sys, os, subprocess, thread

from lib_config import config_class
from lib_socket import socket_class
#from lib_func import udp_server_class

class monitor_class:

    def __init__( self, confilg_file ):
        self.conf_file = config_file
        self.config = config_class( self.conf_file )
        self.sec = self.config.get_sections()
        self.config_dict = self.config.get_all_sections_dict()
        self.monitor_dict = self.config.get_section_dict( "monitor" )

        # self.udp_server_conf_dict = self.config.get_section_dict( "udp_server" )
        # self.udp_server = socket_class()
        # self.udp_server.create_udp_socket()
        # self.udp_server.bind( int(self.udp_server_conf_dict['udp_port']) )
        
    def monitor( self ):
        #thread.start_new_thread( self.udp_server_setup, () )
        
        ui_server = subprocess.Popen( [ "python", self.monitor_dict['ui_server'], str(self.config.get_section_dict("ui_server")) ] )
        scheduler = subprocess.Popen( [ "python", self.monitor_dict['scheduler'], str(self.config.get_section_dict("scheduler")) ] )
        hdlr_lower = subprocess.Popen( [ "python", self.monitor_dict['hdlr_lower'], str(self.config.get_section_dict("hdlr_lower")) ] )
        hdlr_upper = subprocess.Popen( [ "python", self.monitor_dict['hdlr_upper'], str(self.config.get_section_dict("hdlr_upper")) ] )
        hdlr_toggle = subprocess.Popen( [ "python", self.monitor_dict['hdlr_toggle'], str(self.config.get_section_dict("hdlr_toggle")) ] )

        
    def udp_server_setup( self ):
        self.udp_server.listen( int(self.udp_server_conf_dict['udp_no_of_conn']) )
        print "in udp_server_setup"
        
        while True:
            udp_client_conn, addr = self.udp_server.accept()
            if udp_client_conn:
                thread.start_new_thread( self.udp_msg_passing, ( udp_client_conn,))

    def udp_msg_passing( self, udp_client_conn ):

        print "in udp_msg_passsing"
        sec_name = ""
        timeout = int( self.udp_server_conf_dict['udp_socket_timeout'] )
        relaunch_counter = self.config_dict[section_name]['relaunch_limit']
        print "relaunch_counter: ", relaunch_counter
        
        try:
            sec_name, addr = udp_client_conn.recvfrom( 1024 )
            udp_client_conn.set_timeout = ( timeout + 2 )
        except socket.timeout:
            print "Socket timeout exception"
            pass
        
        while True :
            try:
                data, addr = udp_client_conn.recvfrom( 1024 )
                udp_client_conn.set_timeout = ( timeout + 2 )
                
                if "alive" in data:
                    print "alive recieved"
                    continue
            except socket.timeout:
                
                if relaunch_counter <= 0:
                    print "Process %s: number of relaunched reached " %(sec_name)
                    break
                else:
                    relaunch_counter -= 1
                    self.config_dict[section_name]['relaunch_limit'] = relaunch_counter
            
                    relaunch_process = subprocess.Popen( [ "python", self.monitor_dict[section_name], str(self.config.get_section_dict(section_name)) ] )
                    
        del udp_client_conn
                    
if __name__ == '__main__':

    data = sys.argv[1:]
    
    if not data or data[0] == "help":
        print "The monitor program takes a configuration file as a command line paramter.\n\tExample: python monitor.py config_file.conf\n"
        sys.exit(0)

    elif len(data) != 1:
        print "Incorrect number of parameters.\n\t The monitor program takes only one paramter as a command line parameter.\n\t Pass help as a command line parameter to the monitor program to know more about parameters"
        sys.exit(0)
    config_file = str(data[0])            
    mon = monitor_class( config_file )
    mon.monitor()
