

--select result_string from online_competition.students as s, online_competition.tests as t, online_competition.student_test_results as st
--where s.student_id = st.student_id and t.test_id = st.test_id and s.student_name ilike 'savio' and t.test_text_xml ilike 'ds_tree';

select student_name from online_competition.students as s, online_competition.tests as t, online_competition.student_test_results as st
where s.student_id = st.student_id and t.test_id = st.test_id  and t.test_text_xml in ( select * from online_competition.dummy_test_id)
and st.result_percent > 60;
