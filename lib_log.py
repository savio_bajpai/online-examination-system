import time, sys, os, logging as log
from datetime import datetime

class log_class:

        #tmp_var=""
	def __init__( self, string  ):
		self.log_handler = None
                self.log_file_name = string

		log_main = log.getLogger( self.log_file_name )
		log_main.setLevel(log.DEBUG)

		fh = log.FileHandler( self.log_file_name )   #(string + '''".log"''')) #'_log_%d-%m-%Y_%H-%M-%S.log'))
                #why is setLevel set twice? once for log_main and again for fh
		fh.setLevel(log.DEBUG)

		formatter = log.Formatter('%(asctime)s: %(levelname)s: %(message)s')
		fh.setFormatter(formatter)

		log_main.addHandler(fh)
		
		self.log_handler = log_main 
		#return log_main
        '''
        def generate_log_file_name( self, string ):
                
                if os.path.exists(os.getcwd() + "/" + string ) is True:
                        self.log_file_name = string
                else:
                        self.log_file_name = datetime.now().strftime( string + "_log_%d-%m-%Y_%H-%M-%S.log" )
        '''
        
        def get_log_file( self ):
                return self.log_file_name

        def get_log_handler( self ):
                return self.log_handler

	def logs ( self, level, message ):
		
		if level is log.INFO:
			self.log_handler.info ( message )
		elif level is log.DEBUG:
			self.log_handler.debug ( message )
		elif level is log.CRITICAL:
			self.log_handler.critical ( message )
		elif level is log.WARNING:
			self.log_handler.warning ( message )

        def move_log_files( self ): # move one session of a monitor
                path = os.getcwd() + "/logs/" + datetime.now().strftime('log.%d-%m-%Y__%H-%M-%S')

                if not os.path.exists( path ):
                        try:
                                os.makedirs( path )
                        except:
                                sys.exit( 0 )
                                
                        for file in os.listdir( os.getcwd() ):
                                if file.endswith(".log"):
                                        try:
                                                shutil.move(file, path)
                                        except shutil.Error:
                                                print "Error moving log file"
                                                sys.exit( 0 )
                                                
        def move_log_file( self, log_file ): # move single file
                
                if os.path.exists(os.getcwd() + "/" + log_file ) is True:

                        path = os.getcwd() + "/logs/" + datetime.now().strftime('log.%d-%m-%Y__%H-%M-%S')

                        if not os.path.exists(path):
                                try:
                                        os.makedirs(path)
                                except:
                                        sys.exit(0)
                        
                                try:
                                        shutil.move(log_file, path)
                                except shutil.Error:
                                        print "Error moving log file"
                                        sys.exit( 0 )
                                        
	def rmLogFiles( self ):
		for file in os.listdir(os.getcwd()):
			if file.endswith(".log"):
				os.remove(file)

	def __del__( self ):
         #       self.log_handler.handlers = []
                self.log_handler = None
		self.log_file_name = ""
	#	self.log_handler = None
                
