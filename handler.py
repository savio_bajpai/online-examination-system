import socket, thread, time,sys, ast
import lib_functions
from multiprocessing import Process
from lib_socket import socket_class
from lib_handler import handler_class
from lib_config import config_class

class hdlr_class():

    def __init__( self, config_dict, user_fun ):

        self.hdlr_dict = config_dict
        self.user_fun = user_fun
        
        self.hdlr = socket_class()
        self.hdlr.create_tcp_socket()
        self.hdlr.bind( int(self.hdlr_dict["port"]) )
        self.hdlr.listen( int(self.hdlr_dict["no_of_conn"]) )

    def hdlr_server_setup( self ):
        while True:
            hdlr_conn, addr = self.hdlr.accept()
            if hdlr_conn:
                thread.start_new_thread( self.hdlr_client,(hdlr_conn,
                                                           self.hdlr_dict['path'],
                                                           self.hdlr_dict['sch_path'],
                                                           self.user_fun,) )

    def hdlr_client( self, hdlr_conn, hdlr_path, sch_path, user_fun):
        
        data = hdlr_conn.recv(1024)
        tmp_data = data.split(" ")
        #print "data received in hdlr", tmp_data
        if data:
            lib_functions.monitor_dir_hdlr( hdlr_path, sch_path, user_fun )
            #lib_functions.scp_file_transfer( output_file, sch_path )
            #lib_functions.file_handler_class( ).delete_file( output_file )# ?: ask sir
            hdlr_conn.send( 'file_sent' )
            
        hdlr_conn.close()
    
def start_hdlr( dic, user_fun ):
    config_dict = ast.literal_eval( sys.argv[1] )
    hdlr = hdlr_class( config_dict, user_fun )
    hdlr.hdlr_server_setup()

    
