import sys, os, thread
import handler

def do_lower( value ):
    return value.lower()

if __name__ == "__main__":
    handler.start_hdlr( sys.argv[1], do_lower )
