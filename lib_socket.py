import socket, time, sys

class socket_class:

        def __init__( self ):
                self.host = ""#socket.gethostname()
                self.sock = None
                
	def create_tcp_socket( self ):
                self.sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                self.sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

        def create_udp_socket( self, sock = None ):
                self.sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
                self.sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
            
        def connect( self, port ):
                try:
                        self.sock.connect( (self.host, port) )
                except:
                        print "connect: unable to connect to port: ", port 
                        sys.exit(0)

        def bind( self, port ):
                self.sock.bind( (self.host, port) )
        
        def send( self, params ):
                send = self.sock.send( str(params) )
                if send == 0:
                        raise RuntimeError("send: socket connection broken" )

	def recv( self, value = 1024 ):
                return self.sock.recv( value )

        def sendto( self, data, port ):
                self.sock.sendto( data, (self.host, port) )

        def recvfrom( self, value = 1024 ):
                return self.sock.recvfrom( value )
        
        def set_timeout( self, timeout = -1 ):

                if timeout == -1:
                        timeout = socket._GLOBAL_DEFAULT_TIMEOUT

                self.sock.settimeout( timeout )
                
        def accept( self ):
                return self.sock.accept()
        
        def listen( self, no_of_conn ):
                self.sock.listen( no_of_conn )

        def get_host_name( self ):
                return socket.gethostname()
        
	def __del__( self ):
                self.sock.close()
                self.sock = None

'''
nxt = None
nxt = socket_class()
nxt.create_tcp_socket()
nxt.bind( 2566 )
nxt.listen(1)
'''

