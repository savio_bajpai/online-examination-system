import os, sys, time, subprocess
from datetime import datetime

class file_handler_class():

    def __init__( self, path = None ):
        self.path = path
        
    def path_exists( self ):
        try:
            return os.path.exists( self.path )
        except:
            print "Specified Path does not exist"

    def rename_file( self, file_name, username ):
        new_file_name = str( username + "_" + file_name )
        try:
            os.rename( file_name, new_file_name )
        except:
            print "rename_file:Incorrect file specified"

        return new_file_name
    
    def get_file_hdlr( self, f_type ):

        while True:
            for files in os.listdir( self.path ):
                if files.endswith( f_type ):
                    return files
            time.sleep(0.5)

    def get_file_sch( self, f_type, username ):
        
            while True:
                for files in os.listdir( self.path ):
                    if files.startswith( username ) and files.endswith( f_type ):
                        return files
                time.sleep(0.5)

    def read_input_file( self, input_file ):
        try:
            finput = open ( self.path + input_file, 'r' )
            input_lst = finput.readlines()
            finput.close()
        except IOError:
            print "Unable to open, file does not eixst"
        return input_lst

    def write_output_file( self, input_lst, output_file, user_func ):
        try:
            foutput = open( output_file, 'w' )
            for i in input_lst:
                op = user_func(i) 
                foutput.write( op )
            foutput.close()
        except IOError:
            print "Unable to open file"
        return output_file
        
    def generate_file_name( self, string, f_type ):
        return str( datetime.now().strftime( string + "_%d-%m-%Y_%H-%M-%S." + f_type) )

    def delete_file( self, file_to_be_deleted ):
        #print "deleting file:", str(self.path + file_to_be_deleted)
        try:
            os.remove( str(self.path + file_to_be_deleted) )
        except:
            print "lib_functions:delete_file: Incorrect file specified"

def scp_file_transfer( file_to_send, destination_dir ):
    try:
        obj = subprocess.Popen(['scp', file_to_send, destination_dir] )
        status = obj.wait()
        #print "scp_status", status
    except:
        print "lib_functions:scp_file_transfer:"
        
def monitor_dir_hdlr( path, sch_path, user_function ):

    hdlr_obj = file_handler_class( path )
    hdlr_obj.path_exists()
    
    files = hdlr_obj.get_file_hdlr( 'input' )
    input_file_lst = hdlr_obj.read_input_file( files )
    hdlr_obj.delete_file( files )# ?: ask sir
    output_file_name = hdlr_obj.generate_file_name( (files.split('_'))[0] , 'output' )
    output_file = hdlr_obj.write_output_file( input_file_lst, output_file_name, user_function)
    scp_file_transfer( output_file, sch_path )

def monitor_dir_sch( path, username ):

    sch_obj = file_handler_class( path )
    sch_obj.path_exists()
        
    files = sch_obj.get_file_sch( 'output', username )
    return files

