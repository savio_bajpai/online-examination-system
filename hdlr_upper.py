import sys, os, thread
import handler

def do_upper( value ):
    return value.upper()

if __name__ == "__main__":
    handler.start_hdlr( sys.argv[1], do_upper )
