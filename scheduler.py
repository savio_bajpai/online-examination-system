import socket, thread, time, sys, ast

from lib_socket import socket_class
import lib_functions
from lib_config import config_class
from lib_db_func import db_class

class scheduler_class():

    def __init__( self, config_dict ):

	#self.hdlr_port= [ 4281, 4282, 4283 ]
        self.sch_dict = config_dict
        
        self.sch = None
        self.sch = socket_class()
        self.sch.create_tcp_socket()
        self.sch.bind( int(self.sch_dict["port"]) )
        self.sch.listen( int(self.sch_dict["no_of_conn"]) )

        
        self.db = None
        #uncomment this part later
        #self.db = db_class()
        
    def scheduler_setup( self ):
        
        while True:
            sch_conn, addr = self.sch.accept()
            if sch_conn:
                thread.start_new_thread( self.scheduler_client, (sch_conn, self.db ) )

    def scheduler_client( self, sch_conn, db_obj ):

	port  = 0
        sch_data = sch_conn.recv(1024)
        #print "data received in scheduler", sch_data        
        if sch_data:
            tmp_data = sch_data.split(" ")

            #uncomment this part later
            #if db_obj.db_select_query( username ) == 1:
            #    print "valid user"
            #else:
            #    sch_conn.send( "invalid user" )
            #    sch_conn.close()
            #    return
                
            hdlr_path = str(self.sch_dict[str(tmp_data[3] + '_path')])
            #print "sch: username: ", tmp_data[0]
            #print "sch: filename: ", tmp_data[1]
            new_input_file = lib_functions.file_handler_class().rename_file( tmp_data[1], tmp_data[0] )
            #print "sch: new_filename: ", new_input_file
            #print "sch: hdlr_path: ", hdlr_path
            
            lib_functions.scp_file_transfer( new_input_file, hdlr_path)
            
            port = int( self.sch_dict[tmp_data[3]] )
            handler = socket_class()
            handler.create_tcp_socket()
            handler.connect( port )
            handler.send( 'file_sent' )
            #print "input file sent to handler: ",tmp_data[3] 
            hdlr_data = handler.recv( 1024 )
            
            output_file = None
            if hdlr_data:
                output_file = lib_functions.monitor_dir_sch( self.sch_dict['sch_path'], tmp_data[0] )
                #print "sch: output file received from hdlr: ", output_file
                sch_conn.send( output_file )
            del handler
        sch_conn.close()
        lib_functions.file_handler_class(self.sch_dict['sch_path']).delete_file( output_file ) 
        
              
if __name__ == '__main__':    
    config_dict = ast.literal_eval( sys.argv[1] )
    scheduler = scheduler_class( config_dict )
    scheduler.scheduler_setup()
