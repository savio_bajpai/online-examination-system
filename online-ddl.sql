
create schema online_competition;

create sequence online_competition.tests_seq start 1 increment 1 no maxvalue;
create table online_competition.tests( test_id int primary key default nextval('online_competition.tests_seq'),
       	     	    test_text_xml text,
		    test_date date,
		    test_time time,
		    test_duration time );

create sequence online_competition.problems_seq start 9000 increment 1 no maxvalue;
create table online_competition.problems( problem_id int primary key default nextval('online_competition.problems_seq'),
       	     	       problem_text_xml text,
		       sample_problem_test_cases text,
		       eval_problem_test_cases text );
		       
create table online_competition.test_problems( test_id int, foreign key (test_id) references online_competition.tests,
       	     		    problem_id int, foreign key (problem_id) references online_competition.problems, problem_marks int );

create sequence online_competition.students_seq start 13101 increment 1 no maxvalue;
create table online_competition.students( student_id int primary key default nextval('online_competition.students_seq'),
       	     	       student_name varchar(50),
		       student_address text,
		       student_email varchar(30)
		       constraint check_email check ( student_email ~* '^[A-Za-z0-9_]+[.]?[A-Za-z0-9_]+@[A-Za-z0-9]+[.][A-Za-z]+$'),
		       student_mobile varchar(12), 
		       student_status text );

create table online_competition.student_test_results( test_id int, foreign key (test_id) references online_competition.tests,
       	     			   student_id int, foreign key (student_id) references online_competition.students,
				   test_status text,
				   result_percent float,
				   result_string text,
				   primary key( test_id, student_id ) );



create table online_competition.dummy_test_id( id int );

set search_path to online_competition;

insert into online_competition.students( student_name, student_address, student_email, student_mobile, student_status) values
       	    			       ( 'savio', 'Pune', 'saviobajpai10@gmail.com', '9881481390', 'MCA'),
       	    			       ( 'sayali', 'Pune', 'sayaliborade@gmail.com', '9604248335', 'MCA'),
       	    			       ( 'mausami', 'Taloda', 'moni.sethiya@gmail.com', '7767084677', 'MCA'),
       	    			       ( 'abc', 'Nashik', 'abc@gmail.com', '0123456789', 'MSC'),
       	    			       ( 'pqr', 'Mumbai', 'pqr@gmail.com', '2587963415', 'M-Tech');

insert into online_competition.tests( test_text_xml, test_date, test_time, test_duration ) values
       	    			    ( 'ds_tree', '2015-03-25', '05:00:00', '1:00:00' ), 
       	    			    ( 'ds_tree', '2015-03-27', '05:00:00', '1:00:00' ),
       	    			    ( 'ds_tree', '2015-01-25', '05:00:00', '0:30:00' ), 
       	    			    ( 'ds_tree', '2015-01-25', '05:00:00', '0:30:00' ),
       	    			    ( 'ds_tree', '2015-01-25', '05:00:00', '0:45:00' ), 
       	    			    ( 'ds_tree', '2015-01-25', '05:00:00', '0:30:00' ); 

insert into online_competition.problems( problem_text_xml,  sample_problem_test_cases, eval_problem_test_cases ) values
       	    			 	( 'isBst_tree', 'demo', 'eval' ),
       	    			 	( 'inorder_Tree', 'demo', 'eval' ), 
       	    			 	( 'preorder_tree', 'demo', 'eval' ), 
       	    			 	( 'postorder_tree', 'demo', 'eval' ); 
		      
insert into online_competition.test_problems( test_id, problem_id, problem_marks ) values
			    		    ( 1, 9000, 10 ),
					    ( 1, 9001, 5 ),
					    ( 2, 9002, 5 ),
					    ( 2, 9003, 5 ),
					    ( 3, 9000, 5 ),
					    ( 3, 9001, 5 );

insert into  online_competition.student_test_results( test_id, student_id, test_status, result_percent, result_string ) values
       	     					     ( 1, 13101, 'completed', 65.0, 'pass' ),
						     ( 1, 13102, 'completed', 75.0, 'pass' ),
						     ( 1, 13103, 'completed', 85.0, 'pass' ),
						     ( 1, 13104, 'not_compete', 35.0, 'fail' ),
						     ( 2, 13101, 'not_complete', 20.0, 'fail' ),
						     ( 2, 13102, 'completed', 65.0, 'pass' );
       	     					     


select * from online_competition.students;
select * from online_competition.tests;
select * from online_competition.problems;
select * from online_competition.test_problems;
select * from online_competition.student_test_results;
select * from online_competition.dummy_test_id;
