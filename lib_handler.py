import time, sys, logging
from lib_log import log_class
from lib_func import func_class

class handler_class():

    def __init__( self, data, ):

        self.input_file_name = ( data[1] )
        self.delay = int( data[2] )
        self.hdlr = data[3]
        self.output_file = ""
        self.input_lst = []
        
        self.temp = 0
       
    def start( self ):
        self.read_input_file()
        self.output_file = func_class().generate_file_name( self.hdlr, "output")
        
        if "lower" in self.hdlr:
            return(self.lower_case_hdlr())
        elif "upper" in self.hdlr:
            return(self.upper_case_hdlr())
        elif "toggle" in self.hdlr:
            return(self.toggle_case_hdlr())
        
    def read_input_file( self ):
        try:
            finput = open ( self.input_file_name, 'r' )
            self.input_lst = finput.readlines()
            #self.temp = len( self.input_lst )
            finput.close()
        except IOError as e:
            print "Unable to open, file does not eixst"

    def lower_case_hdlr( self ):
        
        foutput = open ( self.output_file, 'a' )
        for lines in self.input_lst:
            op = lines.lower()
            foutput.write( op )
        foutput.close()

        return self.output_file

    def upper_case_hdlr( self ):
        
        foutput = open ( self.output_file, 'a' )
        for lines in self.input_lst:
            op = lines.upper()
            foutput.write( op )
        foutput.close()
        
        return self.output_file

    def toggle_case_hdlr( self ):
       
        foutput = open( self.output_file, 'a' )

        for lines in self.input_lst:
            op = lines.swapcase()
            foutput.write( op )
        foutput.close()

        return self.output_file
