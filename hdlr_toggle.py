import sys, os, thread
import handler

def do_toggle( value ):
    return value.swapcase()

if __name__ == "__main__":
    handler.start_hdlr( sys.argv[1], do_toggle )
