import ConfigParser, sys

class config_class():

    def __init__( self, config_file ):
        self.config_file = config_file
        self.conf = ConfigParser.ConfigParser()
        self.conf.read( self.config_file )
        self.sections = self.conf.sections()
        self.section_dict = None
        
    def get_sections( self ):
        return self.sections
    
    def get_section_dict( self,  section ):
        temp_dict = {}
        options = self.conf.options( section )
        
        for option in options:
            try:
                temp_dict[option] = self.conf.get( section, option )
                if temp_dict[option] == -1:
                    DebugPrint("skip %s" %option )
            except:
                print( "exception on %s! " %option )
                temp_dict[option] = None

        self.section_dict = temp_dict
        return temp_dict

    def get_all_sections_dict( self ):
        temp_dict_dict = {}
        for i in self.sections:
            temp_dict_dict[ i ]  = self.get_section_dict( i )

        return temp_dict_dict
    
    def get_port( self ):#SB1:  may not be needed
        if self.section_dict is None:
            print "Configuration dictionary Empty"
            sys.exit( 0 )
        return int( self.section_dict )

# dic = {}
# obj = None
# obj = config_class( "config_file.conf" )
# sec = obj.get_sections()

# dic = obj.get_all_sections_dict()

# print dic
