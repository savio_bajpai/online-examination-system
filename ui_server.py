import socket, thread, time, ast, sys
from lib_socket import socket_class
#from lib_func import udp_client_msg_passing

class web_server_class():

    def __init__( self, config_dict ):

        self.web_server_dict = config_dict
        
        self.web_server = socket_class()
        self.web_server.create_tcp_socket()
        self.web_server.bind( int(self.web_server_dict['port']) )
        self.web_server.listen( int(self.web_server_dict['no_of_conn']) )
        
    def web_server_setup( self ):

        # udp_client = socket_class()
        # udp_client.create_udp_socket()
        # udp_client.bind( int(self.web_server_dict['udp_port']) )
        # thread.start_new_thread( udp_client_msg_passing, (udp_client, section_name )) # determine how to get section name
        
        while True:
            web_server_conn, addr = self.web_server.accept()
            if web_server_conn:
                thread.start_new_thread( self.web_server_client, (web_server_conn,) )

    def web_server_client( self, web_server_conn ):

        data = web_server_conn.recv( 1024 )
        #print "data received from client", data
        if data:
            sch = None
            sch = socket_class()
            sch.create_tcp_socket()
            sch.connect(2056)
            sch.send(data)
	    data = sch.recv(1024)
            
            if data:
 	           web_server_conn.send(data)
                   
        web_server_conn.close()
              
if __name__ == '__main__':
    
    config_dict = ast.literal_eval( sys.argv[1] )
    web_server = web_server_class( config_dict )
    web_server.web_server_setup()
